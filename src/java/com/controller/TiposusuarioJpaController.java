/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.controller.exceptions.IllegalOrphanException;
import com.controller.exceptions.NonexistentEntityException;
import com.controller.exceptions.RollbackFailureException;
import com.entities.Tiposusuario;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.entities.Usuarios;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author Kevin Lovos
 */
public class TiposusuarioJpaController implements Serializable {

    public TiposusuarioJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Tiposusuario tiposusuario) throws RollbackFailureException, Exception {
        if (tiposusuario.getUsuariosList() == null) {
            tiposusuario.setUsuariosList(new ArrayList<Usuarios>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<Usuarios> attachedUsuariosList = new ArrayList<Usuarios>();
            for (Usuarios usuariosListUsuariosToAttach : tiposusuario.getUsuariosList()) {
                usuariosListUsuariosToAttach = em.getReference(usuariosListUsuariosToAttach.getClass(), usuariosListUsuariosToAttach.getId());
                attachedUsuariosList.add(usuariosListUsuariosToAttach);
            }
            tiposusuario.setUsuariosList(attachedUsuariosList);
            em.persist(tiposusuario);
            for (Usuarios usuariosListUsuarios : tiposusuario.getUsuariosList()) {
                Tiposusuario oldIdTipoUsuarioOfUsuariosListUsuarios = usuariosListUsuarios.getIdTipoUsuario();
                usuariosListUsuarios.setIdTipoUsuario(tiposusuario);
                usuariosListUsuarios = em.merge(usuariosListUsuarios);
                if (oldIdTipoUsuarioOfUsuariosListUsuarios != null) {
                    oldIdTipoUsuarioOfUsuariosListUsuarios.getUsuariosList().remove(usuariosListUsuarios);
                    oldIdTipoUsuarioOfUsuariosListUsuarios = em.merge(oldIdTipoUsuarioOfUsuariosListUsuarios);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Tiposusuario tiposusuario) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Tiposusuario persistentTiposusuario = em.find(Tiposusuario.class, tiposusuario.getId());
            List<Usuarios> usuariosListOld = persistentTiposusuario.getUsuariosList();
            List<Usuarios> usuariosListNew = tiposusuario.getUsuariosList();
            List<String> illegalOrphanMessages = null;
            for (Usuarios usuariosListOldUsuarios : usuariosListOld) {
                if (!usuariosListNew.contains(usuariosListOldUsuarios)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Usuarios " + usuariosListOldUsuarios + " since its idTipoUsuario field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Usuarios> attachedUsuariosListNew = new ArrayList<Usuarios>();
            for (Usuarios usuariosListNewUsuariosToAttach : usuariosListNew) {
                usuariosListNewUsuariosToAttach = em.getReference(usuariosListNewUsuariosToAttach.getClass(), usuariosListNewUsuariosToAttach.getId());
                attachedUsuariosListNew.add(usuariosListNewUsuariosToAttach);
            }
            usuariosListNew = attachedUsuariosListNew;
            tiposusuario.setUsuariosList(usuariosListNew);
            tiposusuario = em.merge(tiposusuario);
            for (Usuarios usuariosListNewUsuarios : usuariosListNew) {
                if (!usuariosListOld.contains(usuariosListNewUsuarios)) {
                    Tiposusuario oldIdTipoUsuarioOfUsuariosListNewUsuarios = usuariosListNewUsuarios.getIdTipoUsuario();
                    usuariosListNewUsuarios.setIdTipoUsuario(tiposusuario);
                    usuariosListNewUsuarios = em.merge(usuariosListNewUsuarios);
                    if (oldIdTipoUsuarioOfUsuariosListNewUsuarios != null && !oldIdTipoUsuarioOfUsuariosListNewUsuarios.equals(tiposusuario)) {
                        oldIdTipoUsuarioOfUsuariosListNewUsuarios.getUsuariosList().remove(usuariosListNewUsuarios);
                        oldIdTipoUsuarioOfUsuariosListNewUsuarios = em.merge(oldIdTipoUsuarioOfUsuariosListNewUsuarios);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tiposusuario.getId();
                if (findTiposusuario(id) == null) {
                    throw new NonexistentEntityException("The tiposusuario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Tiposusuario tiposusuario;
            try {
                tiposusuario = em.getReference(Tiposusuario.class, id);
                tiposusuario.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tiposusuario with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Usuarios> usuariosListOrphanCheck = tiposusuario.getUsuariosList();
            for (Usuarios usuariosListOrphanCheckUsuarios : usuariosListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Tiposusuario (" + tiposusuario + ") cannot be destroyed since the Usuarios " + usuariosListOrphanCheckUsuarios + " in its usuariosList field has a non-nullable idTipoUsuario field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tiposusuario);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Tiposusuario> findTiposusuarioEntities() {
        return findTiposusuarioEntities(true, -1, -1);
    }

    public List<Tiposusuario> findTiposusuarioEntities(int maxResults, int firstResult) {
        return findTiposusuarioEntities(false, maxResults, firstResult);
    }

    private List<Tiposusuario> findTiposusuarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Tiposusuario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Tiposusuario findTiposusuario(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Tiposusuario.class, id);
        } finally {
            em.close();
        }
    }

    public int getTiposusuarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Tiposusuario> rt = cq.from(Tiposusuario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
